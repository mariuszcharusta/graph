import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
    public static void main(String args[]) {
        List<GraphElement> rooms = readRoomsFromFile("C:\\Users\\mchar\\IdeaProjects\\graph\\src\\main\\resources\\roomsDefinition");
        Scanner scanner = new Scanner(System.in);
        Character direction =' ';

        Graph graph = new Graph(rooms);
        System.out.print("In which room you want to start: ");
        for (GraphElement room : rooms) {
            System.out.print(room.getSource() + ' ');
        }

        String currentRoom = scanner.nextLine();
        System.out.println("You are in room " + currentRoom);


        while (!direction.equals('q')) {
            System.out.print("What direction you want to move " + graph.getDirectionsForRoom(currentRoom) + " ? Enter 'q' to finish ");
            direction = scanner.nextLine().charAt(0);
            if (direction.equals('n') || direction.equals('s') || direction.equals('w') || direction.equals('e')) {
                currentRoom = graph.move(currentRoom, direction);
                System.out.println("You are in room " + currentRoom);
            }

        }

    }

    private static List<GraphElement> readRoomsFromFile(String roomsDefinitionFileName) {
        List<GraphElement> rooms = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream(roomsDefinitionFileName);
            Scanner sc = new Scanner(fis);
            while (sc.hasNextLine()) {
                String[] line = sc.nextLine().split(" ");
                GraphElement room = new GraphElement(line[0]);
                for (int i = 1; i < line.length; i++) {
                    String[] directionAndDestination = line[i].split(":");
                    room.addPath(directionAndDestination[0].charAt(0), directionAndDestination[1]);
                }
                rooms.add(room);
            }
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rooms;
    }
}
