import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Graph {
    private List<GraphElement> edges;

    public Graph(List<GraphElement> edges) {
        this.edges = edges;
    }

    public String move(String currentPosition, char direction) {
        for (GraphElement edge : edges) {
            if (edge.getSource().equals(currentPosition) && edge.getDestinationAndDirection().keySet().contains(direction))
                return edge.getDestinationAndDirection().get(direction);
        }
        System.out.println("Wrong direction. You have to stay in the same room.");
        return currentPosition;
    }
     public List<Set<Character>> getDirectionsForRoom(String room){
         List<Set<Character>> availableDirections = edges.stream().filter(r -> r.getSource().equals(room)).map(r -> r.getDestinationAndDirection().keySet()).collect(Collectors.toList());
         return availableDirections;
    }
}