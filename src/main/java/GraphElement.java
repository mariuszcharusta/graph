import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GraphElement {
    private String source;
    private HashMap<Character, String> destinationAndDirection;

    public GraphElement(String source){
        this.source=source;
        this.destinationAndDirection = new HashMap();
    }

    public void addPath(Character direction, String destination){
        destinationAndDirection.put(direction, destination);
    }




}


